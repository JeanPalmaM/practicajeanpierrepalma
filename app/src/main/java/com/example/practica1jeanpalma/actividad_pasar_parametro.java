package com.example.practica1jeanpalma;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class actividad_pasar_parametro extends AppCompatActivity {
    EditText cajaDatos;
    Button buttonEnviar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_pasar_parametro);
        cajaDatos = (EditText) findViewById(R.id.txtParametro);
        buttonEnviar = (Button) findViewById(R.id.buttonEnviarParametro);
        buttonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent  = new Intent(
                        actividad_pasar_parametro.this, actividad_recibir_parametro.class);
                Bundle bundle = new Bundle();
                bundle.putString("dato",cajaDatos.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

    }
}

